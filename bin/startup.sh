#!/usr/bin/env sh

if [[ "$VERBOSE" == "true" ]]; then
	mkdir -p /var/log/nginx
	touch /var/log/nginx/error.log /var/log/nginx/access.log
	tail -f /var/log/nginx/*.log &
fi

#run services
crond start & php-fpm -RD & nginx -g 'daemon off;'
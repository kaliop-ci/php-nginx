FROM php:7.3-fpm-alpine3.11
LABEL Maintainer SG <dt@kaliop.com>

RUN apk --update --no-cache add \
	curl \
	g++ \
	freetype-dev \
	icu \
	icu-dev \
	libjpeg-turbo-dev \
	libpng-dev \
	libxml2-dev \
	libxslt-dev \
	shadow \
	libzip-dev \
	bash \
	mysql-client \
	git

RUN docker-php-ext-configure \
	gd --with-jpeg-dir=/usr/local/ --with-freetype-dir=/usr/include/
RUN docker-php-ext-install \
	gd \
	bcmath \
	intl \
	pdo_mysql \
	soap \
	xsl \
  	sockets \
	zip

# Install nginx packages
RUN apk --no-cache add nginx curl

# Start up services
COPY ./bin/startup.sh /usr/local/bin/startup.sh
RUN chmod +x /usr/local/bin/startup.sh

# Configure nginx
COPY config/nginx/*.conf /etc/nginx/

# Remove default server definition
RUN rm /etc/nginx/conf.d/default.conf

# Add config PHP
COPY config/php.ini /usr/local/etc/php/php.ini

# Install composer
RUN curl -sS https://getcomposer.org/installer | \
  php -- --install-dir=/usr/local/bin --filename=composer

# Setup document root
RUN mkdir -p /var/www/html

# Make the document root a volume
#VOLUME /var/www/html

# Add application
#WORKDIR /var/www/html
#COPY --chown=nobody src/ /var/www/html/

# Expose the port nginx is reachable on
EXPOSE 80


CMD [ "sh", "/usr/local/bin/startup.sh" ]